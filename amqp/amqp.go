package amqp

import (
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	rabbit "github.com/streadway/amqp"
	"strings"
	"time"
)

const exchangeConfig = "skillogs.%s.configuration.e"

type Configurer interface {
	AmqpURI() string
	Environment() string
}

type Emitter struct {
	amqpUri  string
	exchange string
	conn     *rabbit.Connection
	ch       *rabbit.Channel
	error    chan *rabbit.Error
}

func New(conf Configurer) (*Emitter, error) {
	em := &Emitter{
		amqpUri:  conf.AmqpURI(),
		exchange: fmt.Sprintf(exchangeConfig, strings.Replace(conf.Environment(), "/", "-", -1)),
	}
	err := em.connect()
	if err != nil {
		return nil, err
	}
	go em.checkConnection()
	return em, nil
}

func (e *Emitter) connect() error {
	var err error
	e.conn, err = rabbit.Dial(e.amqpUri)
	if err != nil {
		return err
	}
	e.ch, err = e.conn.Channel()
	if err != nil {
		return err
	}
	e.error = e.conn.NotifyClose(make(chan *rabbit.Error))
	return nil
}

func (e *Emitter) Emit(dto interface{}) error {
	body, err := json.Marshal(dto)
	if err != nil {
		return err
	}
	if e.ch != nil {
		return e.ch.Publish(e.exchange, "", false, false, rabbit.Publishing{Body: body})
	}
	return nil
}

func (e *Emitter) Close() error {
	if err := e.ch.Close(); err != nil {
		return err
	}
	e.ch = nil
	if err := e.conn.Close(); err != nil {
		return err
	}
	return nil
}

func (e *Emitter) checkConnection() {
	var err *rabbit.Error
	for {
		err = <-e.error
		if err != nil {
			e.ch = nil
			for {
				logrus.Infof("Rabbit connection closed, trying to reconnect...")
				err := e.connect()
				if err == nil {
					break
				}
				time.Sleep(time.Second * 10)
			}
		}
	}
}
